import Heading from "./components/shared/Heading";
import { Route, Switch } from "react-router-dom";
import "./scss/app.scss";
import ListItems from "./components/shared/ListItems";
import ItemDetail from "./components/shared/ItemDetail";
import { ContextProvider } from "./context/GlobalContext";
function App() {
  return (
    <div className="main-container">
      <ContextProvider>
        <Heading />
        <Switch>
          <Route path="/items" component={ListItems} exact />
          <Route path="/items/:id" component={ItemDetail} exact />
        </Switch>
      </ContextProvider>
    </div>
  );
}

export default App;
