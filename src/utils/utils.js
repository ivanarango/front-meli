export const formatPrice = (currency, amount, decimals) => {
  return amount.toLocaleString("en-US", {
    style: "currency",
    currency,
    maximumFractionDigits: decimals,
  });
};
