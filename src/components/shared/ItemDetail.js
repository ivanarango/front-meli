import { useEffect, useState } from "react";
import "../../scss/ItemDetail.scss";
import { formatPrice } from "../../utils/utils";
import Breadcrumb from "./Breadcrumb";
import { useParams } from "react-router-dom";
import axios from "axios";
import Constants from "../../constants/constants";
import Loading from "./Loading";

const ItemDetail = () => {
  const params = useParams();
  const id = params.id;
  const [item, setItem] = useState({});
  const [categories, setCategories] = useState([]);
  const [hasData, setHasData] = useState(false);
  useEffect(() => {
    axios.get(`${Constants.URL_API+Constants.ITEMS+'/'+id}`).then((res) => {
      setItem(res.data.item);
      setCategories(res.data.categories);
      setHasData(true);
    });
  }, [id]);

  const renderCardDetail = () => {
    return (
      <>
        <Breadcrumb breadcrumb={categories} />
        <div className="detail-item-card">
          <div className="detail-item-card__image-description">
            <img width="100%" height="500px" src={item.picture} alt="item" />
            <p className="description-product-title">
              Descripción del producto
            </p>
            <p className="description">{item.description}</p>
          </div>
          <div className="detail-item-card__information">
            <p className="detail-item-card__information__state-sold">
              {item.condition} - {item.sold_quantity} vendidos
            </p>
            <h1 className="detail-item-card__information__product-title">
              {item.title}
            </h1>
            <p className="detail-item-card__information__price">
              {formatPrice(
                item.price.currency,
                item.price.amount,
                item.price.decimals
              )}
            </p>
            <button className="detail-item-card__information__button">
              <span>Comprar</span>
            </button>
          </div>
        </div>
      </>
    );
  };

  return (
    <div className="detail-item-container">
      {!hasData ? <Loading/> : renderCardDetail()}
    </div>
  );
};

export default ItemDetail;
