import "../../scss/Heading.scss";
import logo from "../../resources/icon.png";
import search from "../../resources/search.png";
import { useState } from "react";
import { useHistory } from "react-router-dom";

const Heading = () => {
  const [query, setQuery] = useState("");
  const history = useHistory();
  const handleChangeQuery = (e) => {
    setQuery(e.target.value);
  };
  const goToGetItems = (e) => {
    e.preventDefault();
    history.push({
      pathname: "/items",
      search: `?search=${query}`,
    });
  };
  return (
    <div>
      <header className="nav-header">
        <img className="primary-icon" src={logo} alt="icon"></img>

        <form className="nav-form" onSubmit={goToGetItems}>
          <input
            type="text"
            className="nav-search-input"
            placeholder="Nunca dejes de buscar"
            onChange={handleChangeQuery}
          ></input>
          <div type="submit" className="search-icon" onClick={goToGetItems}>
            <img src={search} alt="search" />
          </div>
        </form>
      </header>
    </div>
  );
};

export default Heading;
