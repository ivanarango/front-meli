import "../../scss/ListItems.scss";
import freeShipping from "../../resources/free-shipping.png";
import { GlobalContext } from "../../context/GlobalContext";
import { useContext, useEffect, useState } from "react";
import { Link, useLocation } from "react-router-dom";
import Breadcrumb from "./Breadcrumb";
import axios from "axios";
import Constants from "../../constants/constants";
import { formatPrice } from "../../utils/utils";
import Loading from "./Loading";

const ListItems = () => {
  const { items, addManyItems } = useContext(GlobalContext);
  const { search } = useLocation();
  const params = new URLSearchParams(search);
  const query = params.get("search");
  const [breadcumb, setBreadcumb] = useState([]);
  const [hasData, setHasData] = useState(false);

  useEffect(() => {
    setHasData(false);
    const getItems = () => {
      axios
        .get(`${Constants.URL_API + Constants.ITEMS}`, {
          params: {
            q: query,
          },
        })
        .then((res) => {
          setBreadcumb(res.data.categories);
          addManyItems(res.data.items);
          setHasData(true);
        });
    };
    getItems();
  }, [search]);

  const renderList = () => {
    return (
      <>
        <Breadcrumb breadcrumb={breadcumb} />
        <div className="list-items-card">
          {items.map((item) => (
            <Link
              className="item"
              to={`items/${item.id}`}
              style={{ color: "inherit", textDecoration: "inherit" }}
              key={item.id}
            >
              <div className="item-description-container">
                <img
                  className="item-description-container__image"
                  width=""
                  height=""
                  src={item.picture}
                  alt="item"
                />
                <span className="item-description-container__price_title">
                  {formatPrice(
                    item.price.currency,
                    item.price.amount,
                    item.price.decimals
                  )}{" "}
                  &nbsp;
                  <img
                    width="16"
                    height="16"
                    src={freeShipping}
                    alt="delivery"
                    hidden={!item.free_shipping}
                  />
                  <p>{item.title}</p>
                </span>
                <div className="item-description-container__condition">
                  <p>{item.state}</p>
                </div>
              </div>
            </Link>
          ))}
        </div>{" "}
      </>
    );
  };


  return (
    <div className="list-items-container">
      {!hasData ? <Loading/>: renderList()}
    </div>
  );
};

export default ListItems;
