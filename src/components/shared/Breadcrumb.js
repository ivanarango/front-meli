import "../../scss/Breadcrumb.scss";

const Breadcrumb = (props) => {
  const { breadcrumb } = props;

  return (
    <div className="breadcrumb">
      <ul >
        {breadcrumb.map((category) => (
          <li className="breadcrumb__item" key={category}>
            <a href="/" className="breadcrumb__link">
              {category}
            </a>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default Breadcrumb;
