import { createContext, useReducer } from "react";
import appReducer from "./AppReducer";

const initialState = {
  items: [
  ]
};

export const GlobalContext = createContext(initialState);

export const ContextProvider = ({ children }) => {
  const [state, dispatch] = useReducer(appReducer, initialState);

  const addManyItems = (items) => {
    dispatch({ type: "ADD_MANY_ITEMS", payload: items });
  };

  return (
    <GlobalContext.Provider
      value={{
        ...state,
        addManyItems
      }}
    >
      {children}
    </GlobalContext.Provider>
  );
};
