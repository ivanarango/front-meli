export default function appReducer(state, request) {
  switch (request.type) {
    case "ADD_MANY_ITEMS":
      return { ...state, items: request.payload };
    default:
      break;
  }
}
